# Frontend of Status Component Project
This is a front end component that implements and tests custom statuses selection component.

Project is implemented in Vue 3 using Typescript and utilising Vue's Composition API and Vuex for managing state

**Selection component** is implemented as Vue Single File Component and it does not have any dependencies. Code of the component is [here](https://gitlab.com/andres-jarviste/scoro_status_client/-/blob/main/src/components/StatusSelect.vue)

Testing and demonstration application is deployed in Heroku and can be accessed [here](https://ancient-sierra-95788.herokuapp.com/)

Front end projects fetches data from [back-end component](https://gitlab.com/andres-jarviste/scoro_status) that fetches it from Scoro API

## Reusable Statuses Selection Component
Component name: StatusSelect.vue

Component shows chosen selection and allows user to select from the set of given selections.When user makes selection component raises event @input. To process selection subscribe to the event in main component

### Component properties
| Property | Description | Required |
| ---------- | ------------- | --------------------|
| selectedId | Id of the currently selected status | Yes |
| options    | Oject of available selections. Each key in object represents status_id and each value (type `OrderedStatusElement`) is an object with status description (see below). It is defined as type `Statuses` | Yes |
| tabIndex   | tab index of the selection | No |
| style | Style of the display when selection is not open. Allowed values: default, reversed and short | No

### Throwing events
| Event | Description |
| ---------- | ------------- |
| input | Occures when user selects value |

#### Options property example
Options propery object can be made of the data fetched from Scoro API via back-end component. 

Each property in the Options is an object that should have following structure:
```
type OrderedStatusElement = {
  status_id: string;
  status_name: string;
  color: string;
  module: string;
  is_default: number;
  modified_date: string;
  order: number
};
```

#### Example of using the component
```
<StatusSelect
    :selectedId="'pending'"
    :options="projectStatuses"
    :tabIndex="3"
    :style="'reversed'"
    @input="handleProjectStatusChange"
/>
```
projectStatuses - object of statuses
handleProjectStatusChange - handler of imput events

### Used Types
Types used by the component can be imported from `src/interfaces/status.ts`

## Project Setup for local development and testing

### Fetching data from backend
All api calls will be made to back end that is located in the same address. All api calls have prefix api

In development mode front end proejct uses proxy (configured on vite.config.ts) to redirect all api calls to localhost:8000

Before running front end, clone, install and run back-end from backend compoent from [here](https://gitlab.com/andres-jarviste/scoro_status))

### Installing front end 
Clone this repository. Deployment scirpt will work only when **front-end and back end repositories are be under the same main folder**

Install dependecies of the project
```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production and add to deployment package
Following scirpt runs npm build that compiles minified javascript production-ready source code of the front-end and copies it to the back-end repository. 

Thne the back-end project can be published to Heroku.

To run the script

```sh
./deploy_2_heroku.sh
```
 

