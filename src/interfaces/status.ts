export type StatusElement = {
  status_id: string;
  status_name: string;
  color: string;
  module: string;
  is_default: number;
  modified_date: string;
};

export type StatusData = {
  data: Array<StatusElement>;
};

export type OrderedStatusElement = StatusElement & {
  order: number;
};

export type Statuses = {
  [key: string]: OrderedStatusElement;
};

export type Projects = {
  project_id: string;
  no: string;
  project_name: string;
  color: string;
  status: string;
};

export type Tasks = {
  datetime_completed: string;
  status: string;
  event_name: string;
  event_id: string;
  sortorder: number;
};
