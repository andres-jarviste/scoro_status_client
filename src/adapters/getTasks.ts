import type { Tasks } from "../interfaces/status";
export type ResponseData = {
  data: Array<Tasks>;
};

async function getTasks(): Promise<Array<Tasks>> {
  const fetchUrl = "/api/tasks";
  const response = await fetch(fetchUrl);
  if (!response.ok) {
    throw new Error("Error loading tasks");
  }
  const result: ResponseData = await response.json();
  return result.data;
}

export { getTasks };
