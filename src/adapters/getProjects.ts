import type { Projects } from "../interfaces/status";
export type ResponseData = {
  data: Array<Projects>;
};

async function getProjects(): Promise<Array<Projects>> {
  const fetchUrl = "/api/projects";
  const response = await fetch(fetchUrl);
  if (!response.ok) {
    throw new Error("Error loading projects");
  }
  const result: ResponseData = await response.json();
  return result.data;
}

export { getProjects };
