import type { InjectionKey } from "vue";
import { createStore, useStore as baseUseStore, Store } from "vuex";
import type {
  Statuses,
  Projects,
  Tasks,
  OrderedStatusElement,
} from "@/interfaces/status";
import { Mutations } from "./mutation-types";
import actions from "@/store/actions";

// define your typings for the store state
export interface State {
  loadingProjectStatuses: boolean;
  loadingProjects: boolean;
  loadingTasks: boolean;
  lastError: string;
  projectStatuses: Statuses;
  tasksStatuses: Statuses;
  projectData: Array<Projects>;
  tasksData: Array<Tasks>;
}

// define injection key
export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    loadingProjectStatuses: false,
    loadingProjects: false,
    loadingTasks: false,
    lastError: "",
    projectStatuses: {},
    tasksStatuses: {},
    projectData: [],
    tasksData: [],
  } as State,
  mutations: {
    [Mutations.SET_PROJECTS](state, payload: Array<Projects>) {
      state.projectData = payload;
    },
    [Mutations.UPDATE_PROJECT](state, payload: OrderedStatusElement) {
      state.projectData[payload.status_id] = payload;
    },
    [Mutations.SET_TASKS](state, payload: Array<Tasks>) {
      state.tasksData = payload;
    },
    [Mutations.SET_STATUSES_PROJECTS](state, payload: Statuses) {
      state.projectStatuses = { ...payload };
    },
    [Mutations.SET_STATUSES_TASKS](state, payload: Statuses) {
      state.tasksStatuses = payload;
    },
    [Mutations.SET_LOADING_PROJECTS](state, payload: boolean) {
      state.loadingProjects = payload;
    },
    [Mutations.SET_LOADING_TASKS](state, payload: boolean) {
      state.loadingTasks = payload;
    },
    [Mutations.SET_ERROR](state, payload: string) {
      state.lastError = payload;
    },
    [Mutations.SET_NEW_ORDER_PROJECT_STATUSES](
      state,
      payload: Array<{ status_id: string; order: number }>
    ) {
      payload.forEach((elem) => {
        const updatedElem: OrderedStatusElement =
          state.projectStatuses[elem.status_id];
        if (updatedElem) {
          state.projectStatuses[elem.status_id].order = elem.order;
        }
      });
    },
  },
  getters: {
    sortedProjectStates(state) {
      const projectStates = Object.keys(state.projectStatuses).map(
        (key) => state.projectStatuses[key]
      );
      projectStates.sort((a, b) => a.order - b.order);
      return projectStates;
    },
  },
  actions,
});

// define your own `useStore` composition function
export function useStore() {
  return baseUseStore(key);
}
