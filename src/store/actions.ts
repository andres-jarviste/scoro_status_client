//import type { ActionContext } from "vuex";
import type { Commit } from "vuex";
import { Mutations } from "./mutation-types";
import { getProjects } from "@/adapters/getProjects";
import { getStatus } from "@/adapters/getStatus";
import { getTasks } from "@/adapters/getTasks";
import type { OrderedStatusElement } from "@/interfaces/status";

async function fetchProjects({ commit }: { commit: Commit }) {
  commit(Mutations.SET_LOADING_PROJECTS, true);
  try {
    commit(Mutations.SET_ERROR, "");
    const loaded = await getProjects();
    commit(Mutations.SET_PROJECTS, loaded);
  } catch (err) {
    commit(Mutations.SET_ERROR, "Unable to load projects. Try again");
  }
  commit(Mutations.SET_LOADING_PROJECTS, false);
}

async function updateProject(
  { commit }: { commit: Commit },
  projectStatus: OrderedStatusElement
) {
  commit(Mutations.UPDATE_PROJECT, projectStatus);
}

async function fetchTasks({ commit }: { commit: Commit }) {
  commit(Mutations.SET_LOADING_TASKS, true);
  try {
    commit(Mutations.SET_ERROR, "");
    const loaded = await getTasks();
    commit(Mutations.SET_TASKS, loaded);
  } catch (err) {
    console.log("Error loading tasks: " + (<Error>err).toString());
    commit(Mutations.SET_ERROR, "Unable to load tasks. Try again");
  }
  commit(Mutations.SET_LOADING_TASKS, false);
}

async function fetchProjectStatuses({ commit }: { commit: Commit }) {
  try {
    commit(Mutations.SET_ERROR, "");
    const loaded = await getStatus("projects");
    commit(Mutations.SET_STATUSES_PROJECTS, loaded);
  } catch (err) {
    commit(Mutations.SET_ERROR, "Unable to load project statuses. Try again");
  }
}

async function fetchTasksStatuses({ commit }: { commit: Commit }) {
  try {
    commit(Mutations.SET_ERROR, "");
    const loaded = await getStatus("tasks");
    commit(Mutations.SET_STATUSES_TASKS, loaded);
  } catch (err) {
    commit(Mutations.SET_ERROR, "Unable to load tasks statuses. Try again");
  }
}

function setError({ commit }: { commit: Commit }, errorMessage: string) {
  commit(Mutations.SET_ERROR, errorMessage);
}

function reorderProjects(
  { commit }: { commit: Commit },
  newOrder: Array<{ status_id: string; order: number }>
) {
  console.log(newOrder);
  commit(Mutations.SET_NEW_ORDER_PROJECT_STATUSES, newOrder);
}

export default {
  fetchProjects,
  fetchProjectStatuses,
  fetchTasksStatuses,
  fetchTasks,
  setError,
  reorderProjects,
  updateProject,
};
