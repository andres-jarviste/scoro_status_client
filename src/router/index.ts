import { createRouter, createWebHistory } from "vue-router";
import ComponentTestView from "../views/ComponentTestView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "select-component",
      component: ComponentTestView,
    },
    {
      path: "/about",
      name: "about",
      component: () => import("../views/AboutView.vue"),
    },
    {
      path: "/events",
      name: "events",
      component: () => import("../views/EventsView.vue"),
    },
    {
      path: "/projects",
      name: "projects",
      component: () => import("../views/ProjectsView.vue"),
    },
    {
      path: "/projects-statuses",
      name: "project-statuses",
      component: () => import("../views/ProjectStatusesSorting.vue"),
    },
  ],
});

export default router;
